package service;

import com.google.gson.Gson;
import dao.EmployerDao;
import daoimpl.EmployerDaoImpl;
import message.Error;
import request.AddVacancyDtoRequest;
import request.GetWorkerDtoRequest;
import request.RegisterEmployerDtoRequest;
import response.AddVacancyDtoResponse;
import response.GetWorkerDtoResponse;
import response.RegisterEmployerDtoResponse;
import serviceException.ServiceException;
import structures.Employer;
import structures.Vacancy;

/**
 * Класс, которому делегирует действия работодатель класс server.
 */

public class EmployerService {
    /**
     * Объект класса EmployerDao.
     */
    private EmployerDao employerDao;

    /**
     * Объект класса Gson.
     */
    private Gson employerGson;

    /**
     * Констуктор - создает новый объект, чтобы избавиться от дальнейшего повтора кода.
     */
    public EmployerService() {
        this.employerDao = new EmployerDaoImpl();
        this.employerGson = new Gson();
    }

    /**
     * Парсит Json строку, производит валидацию и делегирует дальнейшую работу EmployerDaoImpl.
     * @param requestJsonString - работодатель,
     * @return - ответ по данному действию.
     */
    public RegisterEmployerDtoResponse registerEmployer(String requestJsonString) {
        RegisterEmployerDtoRequest dtoEmployer = employerGson.fromJson(requestJsonString, RegisterEmployerDtoRequest.class);
        if (!dtoEmployer.validate()) {
            return new RegisterEmployerDtoResponse(employerGson.toJson(Error.WRONG_FIELD_STRING));
        }
        Employer employer = new Employer(dtoEmployer.getFirstName(),dtoEmployer.getLastName(),
                dtoEmployer.getMiddelName(),dtoEmployer.getEmail(),dtoEmployer.getLogin(),
                dtoEmployer.getPassword(),dtoEmployer.getCompanyName(),dtoEmployer.getAddress());
        RegisterEmployerDtoResponse registerEmployerDtoResponse;

        try {
            registerEmployerDtoResponse = new RegisterEmployerDtoResponse(employerDao.insert(employer));
        } catch (ServiceException e) {
            return new RegisterEmployerDtoResponse(employerGson.toJson(e.getErrorCode()));
        }

        return registerEmployerDtoResponse;
    }

    /**
     * Парсит Json строку, производит валидацию и делегирует дальнейшую работу EmployerDaoImpl.
     * @param requestJsonString - вакансия,
     * @return - ответ по данному действию.
     */
    public AddVacancyDtoResponse addVacancy(String requestJsonString) {
        AddVacancyDtoRequest vacancyDtoRequest = employerGson.fromJson(requestJsonString, AddVacancyDtoRequest.class);
        if (vacancyDtoRequest.validate()) {
            return new AddVacancyDtoResponse(employerGson.toJson(Error.WRONG_FIELD_STRING));
        }

        Vacancy vacancy = new Vacancy(vacancyDtoRequest.getJobTitle(),vacancyDtoRequest.getSalary(),vacancyDtoRequest.isActive(),
                                      vacancyDtoRequest.getListDemand());
        AddVacancyDtoResponse addVacancyDtoResponse;

        try {
            addVacancyDtoResponse = new AddVacancyDtoResponse(employerDao.addVacancy(vacancy, vacancyDtoRequest.getToken()));
        } catch (ServiceException e) {
            return new AddVacancyDtoResponse(employerGson.toJson(e.getErrorCode()));
        }
        return addVacancyDtoResponse;
    }
    /**
     * Парсит Json строку, производит валидацию и делегирует дальнейшую работу EmployerDaoImpl.
     * @param requestJsonString - запрос о подходящих работниках,
     * @return - ответ по данному действию.
     */
    public GetWorkerDtoResponse getWorker(String requestJsonString) {
        GetWorkerDtoRequest getWorkerDtoRequest = employerGson.fromJson(requestJsonString, GetWorkerDtoRequest.class);
        if (getWorkerDtoRequest.validate()) {
            return new GetWorkerDtoResponse(employerGson.toJson(Error.WRONG_FIELD_STRING));
        }

        GetWorkerDtoResponse getWorkerDtoResponse;
        try {
            getWorkerDtoResponse = new GetWorkerDtoResponse(employerGson.toJson(employerDao.getWorker(getWorkerDtoRequest.getVacancy(),
                                                                                getWorkerDtoRequest.getEnumRequest(), getWorkerDtoRequest.getToken())));
        }catch (ServiceException e) {
            return new GetWorkerDtoResponse(employerGson.toJson(e.getErrorCode()));
        }

        return getWorkerDtoResponse;

    }


}
