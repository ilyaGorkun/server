package service;

import com.google.gson.Gson;
import dao.UserDao;
import daoimpl.UserDaoImpl;
import message.Error;
import request.LoginUserDtoRequest;
import response.LoginUserDtoResponse;
import response.LogoutUserDtoRespones;
import serviceException.ServiceException;
import structures.User;

/**
 * Класс, которому делегирует действия любого пользователя класс server.
 */

public class UserService {
    /**
     * Объект класса UserDao.
     */
    private UserDao userDao;

    /**
     * Объект класса Gson.
     */
    private  Gson userGson;

    /**
     * Констуктор - создает новый объект, чтобы избавиться от дальнейшего повтора кода.
     */
    public UserService() {
        this.userDao = new UserDaoImpl();
        this.userGson = new Gson();
    }

    /**
     * Парсит Json строку, производит валидацию и делегирует дальнейшую работу UserDaoImpl.
     * @param requestJsonString - пользователь,
     * @return - ответ по данному действию.
     */
    public LogoutUserDtoRespones userLogout(String requestJsonString) {
        try {
            userDao.userLogout(userGson.fromJson(requestJsonString, User.class));
        } catch (ServiceException e) {
            return new LogoutUserDtoRespones(userGson.toJson(e.getErrorCode()));
        }

        return new LogoutUserDtoRespones("Done");
    }

    /**
     * Парсит Json строку, производит валидацию и делегирует дальнейшую работу UserDaoImpl.
     * @param requestJsonString - пользователь,
     * @return - ответ по данному действию.
     */
    public LoginUserDtoResponse userLogin(String requestJsonString) {
        LoginUserDtoRequest loginUserDtoRequest = userGson.fromJson(requestJsonString,LoginUserDtoRequest.class);
        if (!loginUserDtoRequest.validate()) {
            return new LoginUserDtoResponse(userGson.toJson(Error.WRONG_FIELD_STRING));
        }
        LoginUserDtoResponse loginUserDtoResponse;
        try{
            loginUserDtoResponse = new LoginUserDtoResponse(userDao.userLogin(userGson.fromJson(requestJsonString, User.class)));
        } catch (ServiceException e) {
            return new LoginUserDtoResponse(userGson.toJson(e.getErrorCode()));
        }
        return loginUserDtoResponse;

    }
}
