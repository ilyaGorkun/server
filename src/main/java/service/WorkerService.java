package service;

import com.google.gson.Gson;
import dao.WorkerDao;
import daoimpl.WorkerDaoImpl;
import message.Error;
import request.AddSkillDtoRequest;
import request.GetVacancyDtoRequest;
import request.RegisterWorkerDtoRequest;
import response.AddSkillDtoResponse;
import response.GetVacancyDtoResponse;
import response.RegisterWorkerDtoResponse;
import serviceException.ServiceException;
import structures.Skill;
import structures.Worker;

/**
 * Класс, которому делегирует действия работника класс server.
 */

public class WorkerService {
    /**
     * Объект класса WorkerDao.
     */
    private WorkerDao workerDao;
    /**
     * Объеект класса Gson.
     */
    private Gson workerGson;

    /**
     * Констуктор - создает новый объект, чтобы избавиться от дальнейшего повтора кода.
     */
    public WorkerService() {
        this.workerDao = new WorkerDaoImpl();
        this.workerGson = new Gson();
    }

    /**
     * Парсит Json строку, производит валидацию и делегирует дальнейшую работу WorkerDaoImpl.
     * @param requestJsonString - работник,
     * @return - ответ по данному действию.
     */
    public RegisterWorkerDtoResponse registerWorker(String requestJsonString) {
        RegisterWorkerDtoRequest dtoWorker = workerGson.fromJson(requestJsonString,RegisterWorkerDtoRequest.class);
        if (dtoWorker.validate()) {
            return new RegisterWorkerDtoResponse(workerGson.toJson(Error.WRONG_FIELD_STRING));
        }

        Worker worker = new Worker(dtoWorker.getFirstName(), dtoWorker.getLastName(),dtoWorker.getMiddelName(),
                                   dtoWorker.getEmail(),dtoWorker.getLogin(),dtoWorker.getPassword(),dtoWorker.isActivity());

        RegisterWorkerDtoResponse registerWorkerDtoResponse;
        try {
                registerWorkerDtoResponse = new RegisterWorkerDtoResponse(workerDao.insert(worker));
        } catch (ServiceException e) {
            return new RegisterWorkerDtoResponse(workerGson.toJson(e.getErrorCode()));
        }

        return registerWorkerDtoResponse;
    }

    /**
     * Парсит Json строку, производит валидацию и делегирует далнейшую работу WorkerDaoImpl.
     * @param requestJsonString - запрос о подходящих вакансиях,
     * @return - ответ по данному действию.
     */
    public GetVacancyDtoResponse getVacancy (String requestJsonString) {
        GetVacancyDtoRequest vacancyDtoRequest = workerGson.fromJson(requestJsonString, GetVacancyDtoRequest.class);

        if(!vacancyDtoRequest.validate()) {
            return new GetVacancyDtoResponse(workerGson.toJson(Error.WRONG_FIELD_STRING));
        }
        GetVacancyDtoResponse getVacancyDtoResponse;
        try {
            getVacancyDtoResponse = new GetVacancyDtoResponse(workerGson.toJson(workerDao.getVacancy(vacancyDtoRequest.getListSkill(),vacancyDtoRequest.getEnumRequest(), vacancyDtoRequest.getToken())));
        } catch (ServiceException e) {
            return new GetVacancyDtoResponse(workerGson.toJson(e.getErrorCode()));
        }

        return getVacancyDtoResponse;
    }
    /**
     * Парсит Json строку, производит валидацию и делегирует далнейшую работу WorkerDaoImpl.
     * @param requestJsonString - скилл,
     * @return - ответ по данному действию.
     */
    public AddSkillDtoResponse addSkill(String requestJsonString) {
        AddSkillDtoRequest skillDtoRequest = workerGson.fromJson(requestJsonString, AddSkillDtoRequest.class);
        if (!skillDtoRequest.validate()) {
            return new AddSkillDtoResponse(workerGson.toJson(Error.WRONG_FIELD_STRING));
        }

        Skill skill = new Skill(skillDtoRequest.getSkillName(),skillDtoRequest.getLevel());

        AddSkillDtoResponse addSkillDtoResponse;

        try {
            addSkillDtoResponse = new AddSkillDtoResponse(workerGson.toJson(workerDao.addSkill(skill,skillDtoRequest.getToken())));
        } catch (ServiceException e) {
            return new AddSkillDtoResponse(workerGson.toJson(e.getErrorCode()));
        }
        return addSkillDtoResponse;
    }

}
