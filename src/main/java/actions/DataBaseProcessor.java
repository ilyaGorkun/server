package actions;

import com.google.gson.Gson;
import data.DataBase;

import java.io.*;

/**
 * Класс занимается сериализацией и десериализацией БД.
 */
public class DataBaseProcessor {
    /**
     * Сериализация БД в формат Json.
     * @param dataBase - БД,
     * @return - срока Json.
     */
    private static String serializeDataBaseToJsonString(DataBase dataBase) {
        return new Gson().toJson(dataBase);
    }

    /**
     * Десериализация Json в БД.
     * @param json - json строка,
     * @return - БД.
     */
    private static DataBase deserializeDatabaseFromJsonString(String json) {
        return new Gson().fromJson(json, DataBase.class);
    }

    /**
     * Сериализация БД в файл.
     * @param savedDataFileName - имя файла,
     * @param dataBase - БД.
     */
    public static void  serializeDatabaseToJsonFile(String savedDataFileName, DataBase dataBase) {
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(savedDataFileName))){
            bw.write(serializeDataBaseToJsonString(dataBase));
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Десериализация из файла БД.
     * @param savedDataFileName - имя файла,
     * @return - БД.
     */
    public static DataBase deserializeDataBaseFromJsonFile(String savedDataFileName){
        try(BufferedReader br = new BufferedReader(new FileReader(savedDataFileName))) {
            return deserializeDatabaseFromJsonString(br.readLine());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            // этот код вызывает ошибку компилятора
            // Internal compiler error: java.lang.ArrayIndexOutOfBoundsException
            // нужно пофиксить :-)
        } finally {
            return new DataBase();
        }
    }

}
