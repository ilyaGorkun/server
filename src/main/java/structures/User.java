package structures;

/**
 * Абстрактный класс модели пользователя.
 */
abstract public class User {

    /**
     * Текстовое поле, имя.
     */
    private String firstName;

    /**
     * Текстовое поле, фамиилия.
     */
    private String lastName;

    /**
     * Текстовое поле, отчество.
     */
    private String middelName;

    /**
     * Текстовое поле, почта.
     */
    private String email;

    /**
     * Текстовое поле, логин.
     */
    private String login;

    /**
     * Текстовое поле, пароль.
     */
    private String password;

    /**
     * Конструктор - создание нового объекта с определенными значениями.
     * @param firstName - имя,
     * @param lastName - фамилия,
     * @param middelName - отчество,
     * @param email - почта,
     * @param login - логин,
     * @param password - пароль.
     */
    public User(String firstName, String lastName, String middelName, String email, String login, String password) {
        setFirstName(firstName);
        setLastName(lastName);
        setMiddelName(middelName);
        setEmail(email);
        this.login = login;
        setPassword(password);
    }

    /**
     * Метод, который изменят поле {@link User#firstName}.
     * @param firstName - имя.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Метод, который изменят поле {@link User#lastName}.
     * @param lastName - фамилия.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    /**
     * Метод, который изменят поле {@link User#middelName}.
     * @param middelName - отчество.
     */
    public void setMiddelName(String middelName) {
        this.middelName = middelName;
    }

    /**
     * Метод, который изменят поле {@link User#email}.
     * @param email - почта.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Метод, который изменят поле {@link User#password}.
     * @param password - пароль.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Метод получения значения поля {@link User#firstName}.
     * @return возвращает имя.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Метод получения значения поля {@link User#lastName}.
     * @return возвращает фамилия.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Метод получения значения поля {@link User#middelName}.
     * @return возвращает отчество.
     */
    public String getMiddelName() {
        return middelName;
    }

    /**
     * Метод получения значения поля {@link User#email}.
     * @return возвращает почта.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Метод получения значения поля {@link User#login}.
     * @return возвращает логин.
     */
    public String getLogin() {
        return login;
    }
    /**
     * Метод получения значения поля {@link User#password}.
     * @return возвращает пароль.
     */
    public String getPassword() {
        return password;
    }

}
