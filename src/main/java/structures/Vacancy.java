package structures;

import java.util.List;
import java.util.Objects;

/**
 * Класс модели вакансии
 */
public class Vacancy {
    /**
     * Текстовое поле, назание вакансии.
     */
    private String jobTitle;

    /**
     * Поле целого типа, стоимость.
     */
    private int salary;

    /**
     * Булевский тип, актуальность вакансии
     */
    private boolean active;

    /**
     * Список, список требований к этой вакансии
     */
    private List<Demand> listDemand;

    /**
     * Конструкор - создание нового объекта с определенными значениями.
     * @param jobTitle - название,
     * @param salary - стоимость,
     * @param active - актуальность,
     * @param listDemand - список требований.
     */

    public Vacancy(String jobTitle, int salary, boolean active, List<Demand> listDemand) {
        setJobTitle(jobTitle);
        setSalary(salary);
        setActive(active);
        this.listDemand = listDemand;
    }

    /**
     * Метод, который изменяет поле {@link Vacancy#active}.
     * @param active - актуальность.
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Метод, который изменят поле {@link Vacancy#jobTitle}.
     * @param jobTitle - название.
     */
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    /**
     * Метод, который изменяет поле {@link Vacancy#salary}.
     * @param salary - стоимость.
     */
    public void setSalary(int salary) {
        this.salary = salary;
    }

    /**
     * Метод получения значения поля {@link Vacancy#jobTitle}.
     * @return возвращает название вакансии.
     */
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     * Метод получения значения поля {@link Vacancy#active}.
     * @return возвращает статус актуальности.
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Метод получения значения поля {@link Vacancy#salary}.
     * @return возвращает стоимость.
     */
    public int getSalary() {
        return salary;
    }

    /**
     * Метод получения значения поля {@link Vacancy#listDemand}.
     * @return возвращает список требований.
     */
    public List<Demand> getListDemand() {
        return listDemand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vacancy vacancy = (Vacancy) o;
        return salary == vacancy.salary &&
                active == vacancy.active &&
                Objects.equals(jobTitle, vacancy.jobTitle) &&
                Objects.equals(listDemand, vacancy.listDemand);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jobTitle, salary, active, listDemand);
    }
}
