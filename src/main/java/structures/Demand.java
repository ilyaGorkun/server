package structures;

import java.util.Objects;

/**
 * Класс модели требования.
 */
public class Demand extends Skill {
    /**
     * Булевский тип, обязательность требования.
     */
    private boolean required;

    /**
     * Конструктор - создает новый объект с определенными значениями.
     * @param skillName - название требования,
     * @param level - уровнь владения,
     * @param required - обязательность.
     */
    public Demand(String skillName, int level, boolean required) {
        super(skillName, level);
        setRequired(required);
    }
    /**
     * Метод получения значения поля {@link Demand#required}.
     * @return возвращает обязательность требования.
     */
    public boolean isRequired() {
        return required;
    }

    /**
     * Метод, который изменяет состояние обязательности требования.
     * @param required - обязательность требования.
     */
    public void setRequired(boolean required) {
        this.required = required;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Demand demand = (Demand) o;
        return required == demand.required;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), required);
    }
}
