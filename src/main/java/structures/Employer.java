package structures;



import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Класс модели работодателя
 */
public class Employer extends User {

    /**
     * Текстовое поле, название компании.
     */
    private String companyName;
    /**
     * Текстовое поле, адрес.
     */
    private String address;
    /**
     * Список, списк вакансий.
     */
    private List<Vacancy> listVacancy;

    /**
     * Конструктор - создание нового объекта с определенными значениями.
     * @param firstName -  имя,
     * @param lastName -  фамилия,
     * @param middelName - отчество,
     * @param email - почта,
     * @param login - логин,
     * @param password - пароль,
     * @param companyName - название компании,
     * @param address - адрес.
     */
    public Employer(String firstName, String lastName, String middelName, String email, String login, String password, String companyName, String address) {
        super(firstName, lastName, middelName, email, login, password);
        this.companyName = companyName;
        this.address = address;
        listVacancy = new ArrayList<>();
    }


    /**
     * Метод, который изменяет поле {@link Employer#companyName}.
     * @param companyName - название компании.
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * Метод, который изменяет поле {@link Employer#address}.
     * @param address - адрес.
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Метод получения значения поля {@link Employer#companyName}.
     * @return возвращает название компании.
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Метод получения значения поля {@link Employer#address}.
     * @return возвращает адрес.
     */
    public String getAddress() {
        return address;
    }

    /**
     * Метод получения значения поля {@link Employer#listVacancy}.
     * @return возвращает список вакансий.
     */
    public List<Vacancy> getListVacancy() {
        return listVacancy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employer employer = (Employer) o;
        return Objects.equals(companyName, employer.companyName) &&
                Objects.equals(address, employer.address) &&
                Objects.equals(listVacancy, employer.listVacancy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyName, address, listVacancy);
    }
}
