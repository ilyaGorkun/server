package structures;

import java.util.Objects;

/**
 * Класс модели навык.
 */
public class Skill {
    /**
     * Текстовое поле, название навыка.
     */
    private String skillName;
    /**
     * Поле целого типа, уровень владения.
     */
    private int level;

    /**
     * Конструктор - создание нового объекта с определенными значениями.
     * @param skillName - имя,
     * @param level - уровень владения.
     */
    public Skill(String skillName, int level) {
        setSkillName(skillName);
        setLevel(level);
    }
    /**
     * Метод, который изменяет поле {@link Skill#skillName}.
     * @param skillName - название.
     */
    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }


    /**
     * Метод, который изменяет поле {@link Skill#level}.
     * @param level - актуальность.
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * Метод получения значения поля {@link Skill#skillName}.
     * @return возвращает название.
     */
    public String getSkillName() {
        return skillName;
    }

    /**
     * Метод получения значения поля {@link Skill#level}.
     * @return возвращает уровень.
     */
    public int getLevel() {
        return level;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Skill skill = (Skill) o;
        return level == skill.level &&
                Objects.equals(skillName, skill.skillName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(skillName, level);
    }
}
