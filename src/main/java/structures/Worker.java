package structures;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс модели работника.
 */

public class Worker extends User {

    /**
     * Булевский тип, который отвечает за состояние работника в общей базе.
     */
    private boolean activity;

    /**
     * список всех возможных навыков работника(Skills).
     */
    private List<Skill> listSkill;

    /**
     * Конструктор - создание нового объекта с определенными значениями.
     *
     * @param firstName - имя,
     * @param lastName - фамилия,
     * @param middelName - отчество,
     * @param email - почта,
     * @param login - логин,
     * @param password - пароль,
     * @param activity - состояние активности.
     */
    public Worker(String firstName, String lastName, String middelName, String email, String login, String password, boolean activity) {
        super(firstName, lastName, middelName, email, login, password);
        this.activity = activity;
        listSkill = new ArrayList<>();
    }

    /**
     * Метод, который изменяет состояние статуса.
     *
     * @param activity - статус активности.
     */
    public void setActivity(final boolean activity) {
        this.activity = activity;
    }

    /**
     * Функция получения значения поля {@link Worker#activity}.
     *
     * @return возвращает статус работник в данный момент.
     */
    public boolean isActivity() {
        return activity;
    }

    /**
     * Функция получения значения поля {@link Worker#listSkill}.
     *
     * @return возвращает список навыков работника.
     */
    public List<Skill> getListSkill() {
        return listSkill;
    }
}
