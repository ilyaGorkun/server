package response;

/**
 * Абстрактный класс, результат действий пользователя.
 */
abstract public class StringDtoResponse {
    /**
     * Текстовое поле, результат дейтсивий.
     */
    private String result;

    /**
     * Конструкто - создание нового объекта сопределенными значениями.
     * @param result - резульат действий.
     */
    public StringDtoResponse(String result) {
        this.result = result;
    }

    /**
     * Метод получения значения поля {@link StringDtoResponse#result}.
     * @return возвращает результат.
     */
    public String getResult() {
        return result;
    }
}
