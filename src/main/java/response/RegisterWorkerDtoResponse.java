package response;

/**
 * Класс, который хранит резульатат действий.
 */
public class RegisterWorkerDtoResponse extends StringDtoResponse {

    public RegisterWorkerDtoResponse(String result) {
        super(result);
    }
}
