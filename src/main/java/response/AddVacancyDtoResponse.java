package response;
/**
 * Класс, который хранит резульатат действий.
 */
public class AddVacancyDtoResponse extends StringDtoResponse {

    public AddVacancyDtoResponse(String result) {
        super(result);
    }

}
