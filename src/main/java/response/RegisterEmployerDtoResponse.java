package response;

/**
 * Класс, который хранит резульатат действий.
 */
public class RegisterEmployerDtoResponse extends StringDtoResponse {

    public RegisterEmployerDtoResponse(String result) {
        super(result);
    }
}
