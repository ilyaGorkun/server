package response;

/**
 * Класс, который хранит резульатат действий.
 */
public class LogoutUserDtoRespones extends StringDtoResponse {

    public LogoutUserDtoRespones(String result) {
        super(result);
    }

}
