package response;
/**
 * Класс, который хранит резульатат действий.
 */
public class GetWorkerDtoResponse extends StringDtoResponse {
    public GetWorkerDtoResponse(String result) {
        super(result);
    }
}
