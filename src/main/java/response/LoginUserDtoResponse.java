package response;

/**
 * Класс, который хранит резульатат действий.
 */
public class LoginUserDtoResponse extends StringDtoResponse {

    public LoginUserDtoResponse(String result) {
        super(result);
    }
}
