package response;
/**
 * Класс, который хранит резульатат действий.
 */
public class AddSkillDtoResponse extends StringDtoResponse {

    public AddSkillDtoResponse(String result) {
        super(result);
    }

}
