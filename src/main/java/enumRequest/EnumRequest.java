package enumRequest;

/**
 * Класс перечисляемых типов для запрсов по вакансиям и работникам.
 */
public enum EnumRequest {
        REQUEST_GET_NOT_LESS,
        REQUEST_WITH_REQUIREMENT,
        REQUEST_GET_ALL,
        REQUEST_GET_NOT_LESS_SORTED,
        REQUEST_AT_LEAST_ONE
}
