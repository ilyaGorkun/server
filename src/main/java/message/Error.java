package message;

/**
 * Класс перечисляемых типов для исключений.
 */
public enum Error {
    WRONG_FIELD_STRING("Wrong format"),
    WRONG_WORKER_EXISTS("Worker exists in Database"),
    WRONG_EMPLOYER_EXISTS("Employer exists in Database"),
    WRONG_WORKER_NOT_EXISTS("Worker not exists in Database "),
    WRONG_EMPLOYER_NOT_EXISTS("Employer not exists in Database "),
    WRONG_LOGIN_IS_EXISTS("Login is exists"),
    WRONG_FALSE_TOKEN("Login is not exists"),
    WRONG_LOGOUT("Error logout"),
    WRONG_LOGIN_NOT_EXISTS("Login not exists in Database "),
    WRONG_VACANCY_EXISTS("Vacancy is exists"),
    WRONG_SKILL_EXISTS("Skill is exists");
    private String errorString;


    Error(String errorString) {
        this.errorString = errorString;
    }

    public String getErrorString() {
        return errorString;
    }
}
