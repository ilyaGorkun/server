package dao;

import enumRequest.EnumRequest;
import serviceException.ServiceException;
import structures.Employer;
import structures.Vacancy;
import structures.Worker;

import java.util.List;

/**
 * Интерфейс DAO для Employer.
 */
public interface EmployerDao {
    /**
     * Метод вставляет работодателя в DataBase.
     * @param employer - работник,
     * @return - токен.
     */
    String insert(Employer employer) throws ServiceException;

    /**
     * Метод добавляет вакансию к нужному работодателю в DataBase.
     * @param vacancy - вакансия,
     * @param token - токен,
     * @return - результат добавления.
     */
    String addVacancy(Vacancy vacancy, String token) throws ServiceException;

    /**
     * Метод получает список подходящих вакансий из DataBase.
     * @param vacancy - вакансия,
     * @param enumRequest - тип запроса,
     * @param token - токен,
     * @return - список работников.
     * @throws ServiceException
     */
    List<Worker> getWorker(Vacancy vacancy, EnumRequest enumRequest, String token) throws ServiceException;
}
