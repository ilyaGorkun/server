package dao;



import enumRequest.EnumRequest;
import serviceException.ServiceException;
import structures.Skill;
import structures.Vacancy;
import structures.Worker;

import java.util.List;

/**
 * Интерйефс DAO для Worker.
 */
public interface WorkerDao {
    /**
     * Метод вставляет работника в DataBase.
     * @param worker - работник,
     * @return - токен.
     */
    String insert(Worker worker) throws ServiceException;

    /**
     * Метод получает подходящие вакансии из DataBase.
     * @param listSkill - список навыков,
     * @param enumRequest - тип запроса,
     * @param token - токен,
     * @return -  список вакансий.
     */
    List<Vacancy> getVacancy(List<Skill> listSkill, EnumRequest enumRequest, String token) throws ServiceException;

    /**
     * Метод добавляет навык к нужному работнику.
     * @param skill - навык,
     * @param token - токен,
     * @return - результат действия.
     */
    String addSkill(Skill skill, String token) throws ServiceException;
}
