package dao;


import serviceException.ServiceException;
import structures.User;

/**
 * Интерфейс DAO для User.
 */
public interface UserDao {
    /**
     * Метод для выхода из сервера пользователю.
     * @param user - пользоваьель.
     */
    void userLogout(User user) throws ServiceException;

    /**
     * Метод для входа в сервер пользователя.
     * @param user - пользователь,
     * @return - токен.
     */
    String userLogin(User user) throws ServiceException;

}
