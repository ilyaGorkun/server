package server;

import actions.DataBaseProcessor;
import data.DataBase;
import data.SingDataBase;
import service.EmployerService;
import service.UserService;
import service.WorkerService;

/**
 * Основной класс, который делегирует всеми основными действиями на сервере.
 */
public class Server {
    /**
     * Объект типа EmployerService.
     */
    private EmployerService employerService;
    /**
     * Объект типа WorkerService.
     */
    private WorkerService workerService;
    /**
     * Объект типа UserService.
     */
    private UserService userService;

    /**
     * Метод создает одиночку и делегирует ему создания DataBase.
     * @param savedDataFileName
     */
    public void startServer(String savedDataFileName) {
        SingDataBase sDataBase = SingDataBase.getSingDatabase();
        sDataBase.addDataBase(savedDataFileName);

    }

    /**
     * Метод произвдит сериализацию в файл объекта DataBase в Json формате.
     * @param savedDateFileName - имя файла,
     * @param dataBase - DataBase в формате Json.
     */
    public void stopServer(String savedDateFileName, DataBase dataBase) {
        DataBaseProcessor.serializeDatabaseToJsonFile(savedDateFileName, dataBase);
    }

    /**
     * Метод делегирует регистрацию в класс EmployerService.
     * @param requestJsonString - работодатель,
     * @return - токен пользователя.
     */
    public String registerEmployer(String requestJsonString) {
        return employerService.registerEmployer(requestJsonString).getResult();
    }

    /**
     * Метод делегирует регистрацию в класс WorkerService.
     * @param requestJsonString - работник,
     * @return - токен пользователя.
     */
    public String registerWorker(String requestJsonString) {
        return workerService.registerWorker(requestJsonString).getResult();
    }
    /**
     * Метод делегирует добавление вакансии в класс EmployerService.
     * @param requestJsonString - вакансия
     * @return - результат о добавление.
     */
    public String addVacancy(String requestJsonString) {
        return employerService.addVacancy(requestJsonString).getResult();
    }
    /**
     * Метод делегирует добавления навыка в класс WorkerService.
     * @param requestJsonString - навык,
     * @return - результат о добавление.
     */
    public String addSkill(String requestJsonString) {
        return  workerService.addSkill(requestJsonString).getResult();
    }

    /**
     * Метод делегирует получение вакансий в класс WorkerService.
     * @param requestJsonString - запрос о подходящих вакансиях,
     * @return - список возмажных вакансий в формате Json.
     */
    public String getVacancies(String requestJsonString) {
        return  workerService.getVacancy(requestJsonString).getResult();
    }
    /**
     * Метод делегирует выход в класс UserService.
     * @param requestJsonString - запрос о выходе,
     * @return - реузультат выхода.
     */
    public String logout(String requestJsonString) {
        return userService.userLogout(requestJsonString).getResult();
    }
    /**
     * Метод делегирует вход в класс UserService.
     * @param requestJsonString - данные пользователя,
     * @return - токен.
     */
    public String login(String requestJsonString) {
        return userService.userLogin(requestJsonString).getResult();
    }





}
