package request;


import structures.Demand;

import java.util.List;

/**
 * Временный класс, аналог Vacancy, для добавления вакансии.
 */
public class AddVacancyDtoRequest {
    private String jobTitle;
    private int salary;
    private boolean active;
    private List<Demand> listDemand;
    private String token;

    public AddVacancyDtoRequest(String jobTitle, int salary, boolean active, List<Demand> listDemand, String token) {
        this.jobTitle = jobTitle;
        this.salary = salary;
        this.active = active;
        this.listDemand = listDemand;
        this.token = token;
    }

    public boolean validate() {
        if (jobTitle == null || salary <= 0 || listDemand == null || token == null) {
            return false;
        }

        if ( jobTitle.equals("") || token.equals("")) {
            return false;
        }

        return true;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<Demand> getListDemand() {
        return listDemand;
    }

    public void setListDemand(List<Demand> listDemand) {
        this.listDemand = listDemand;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
