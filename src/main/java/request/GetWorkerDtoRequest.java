package request;

import enumRequest.EnumRequest;
import structures.Demand;
import structures.Vacancy;
/**
 * Временный класс, запрос по по вакансии.
 */
public class GetWorkerDtoRequest {
    private Vacancy vacancy;
    private EnumRequest enumRequest;
    private String token;

    public GetWorkerDtoRequest(Vacancy vacancy, EnumRequest enumRequest, String token) {
        this.vacancy = vacancy;
        this.enumRequest = enumRequest;
        this.token = token;
    }

    public boolean validate() {
        if (vacancy == null) {
            return false;
        }

        if (token == null || token.equals("")) {
            return false;
        }

        if (enumRequest == null) {
            return false;
        }

        for (Demand demand: vacancy.getListDemand()) {
            if (demand == null) {
                return false;
            }
        }

        return true;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public EnumRequest getEnumRequest() {
        return enumRequest;
    }

    public void setEnumRequest(EnumRequest enumRequest) {
        this.enumRequest = enumRequest;
    }

    public Vacancy getVacancy() {
        return vacancy;
    }

    public void setVacancy(Vacancy vacancy) {
        this.vacancy = vacancy;
    }
}
