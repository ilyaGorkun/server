package request;

import structures.Vacancy;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
/**
 * Временный класс, аналог Employer, для регистрации.
 */
public class RegisterEmployerDtoRequest {
    private String companyName;
    private String address;
    private String login;
    private String firstName;
    private String lastName;
    private String middelName;
    private String password;
    private String email;
    private List<Vacancy> listVacancy;

    public RegisterEmployerDtoRequest(String companyName, String address,String login ,String firstName, String lastName,
                                      String middelName, String password, String email, List<Vacancy> listVacancy) {
        this.companyName = companyName;
        this.address = address;
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middelName = middelName;
        this.password = password;
        this.email = email;
        this.listVacancy = listVacancy;
    }

    public boolean validate() {
        if (this.firstName == null || this.lastName == null || this.middelName == null ||
            this.companyName == null || this.address == address || this.email == null || this.password == null || this.login == null) {
            return false;
        }

        if (this.firstName.equals("") || this.lastName.equals("") || this.middelName.equals("") ||
                this.companyName.equals("") || this.address.equals("") || this.email.equals("") || this.password.equals("") || this.login.equals("")) {
            return false;
        }
        return true;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddelName() {
        return middelName;
    }

    public void setMiddelName(String middelName) {
        this.middelName = middelName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setListVacancy(List<Vacancy> listVacancy) {
        this.listVacancy = listVacancy;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getAddress() {
        return address;
    }

    public List<Vacancy> getListVacancy() {
        return listVacancy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegisterEmployerDtoRequest that = (RegisterEmployerDtoRequest) o;
        return Objects.equals(companyName, that.companyName) &&
                Objects.equals(address, that.address) &&
                Objects.equals(listVacancy, that.listVacancy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyName, address, listVacancy);
    }
}
