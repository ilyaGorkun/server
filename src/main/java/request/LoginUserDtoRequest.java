package request;

import java.util.Objects;

/**
 * Временный класс, аналог User, для входа в систему.
 */
public class LoginUserDtoRequest {
    private String firstName;
    private String lastName;
    private String middelName;
    private String email;
    private String login;
    private String password;

    public LoginUserDtoRequest(String firstName, String lastName, String middelName, String email, String login, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middelName = middelName;
        this.email = email;
        this.login = login;
        this.password = password;
    }

    public boolean validate() {
        if (this.firstName == null || this.lastName == null || this.middelName == null || this.email == null || this.password == null || this.login == null) {
            return false;
        }

        if (this.firstName.equals("") || this.lastName.equals("") || this.middelName.equals("") ||
                  this.email.equals("") || this.password.equals("") || this.login.equals("")) {
            return false;
        }
        return true;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddelName() {
        return middelName;
    }

    public void setMiddelName(String middelName) {
        this.middelName = middelName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoginUserDtoRequest that = (LoginUserDtoRequest) o;
        return Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(middelName, that.middelName) &&
                Objects.equals(email, that.email) &&
                Objects.equals(login, that.login) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, middelName, email, login, password);
    }
}
