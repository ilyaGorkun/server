package request;


import enumRequest.EnumRequest;
import structures.Skill;

import java.util.List;

/**
 * Временный класс, запрос работников.
 */
public class GetVacancyDtoRequest {
    List<Skill> listSkill;
    EnumRequest enumRequest;
    String token;

    public GetVacancyDtoRequest(List<Skill> listSkill, EnumRequest enumRequest, String token) {
        this.listSkill = listSkill;
        this.enumRequest = enumRequest;
        this.token = token;
    }

    public boolean validate() {
        for (Skill skill: listSkill) {
            if (skill == null) {
                return false;
            }
        }
        return enumRequest != null && token != null && !token.equals("");
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<Skill> getListSkill() {
        return listSkill;
    }

    public void setListSkill(List<Skill> listSkill) {
        this.listSkill = listSkill;
    }

    public EnumRequest getEnumRequest() {
        return enumRequest;
    }

    public void setEnumRequest(EnumRequest enumRequest) {
        this.enumRequest = enumRequest;
    }
}
