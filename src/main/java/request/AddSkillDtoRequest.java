package request;

/**
 * Временный класс, аналог Skill, для добавления навыков.
 */
public class AddSkillDtoRequest {
    private String skillName;
    private int level;
    private String token;

    public AddSkillDtoRequest(String skillName, int level, String token) {
        this.skillName = skillName;
        this.level = level;
        this.token = token;
    }

    public boolean validate() {
        if(skillName== null || level <= 0 || token == null) {
            return false;
        }

        if (skillName.equals("") || token.equals("")) {
            return false;
        }

        return true;
    }

    public String getSkillName() {
        return skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
