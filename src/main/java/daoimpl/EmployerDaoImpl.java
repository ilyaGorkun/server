package daoimpl;

import dao.EmployerDao;
import data.SingDataBase;
import enumRequest.EnumRequest;
import serviceException.ServiceException;
import structures.Employer;
import structures.Vacancy;
import structures.Worker;

import java.util.List;

/**
 * Класс имплементирует интерфейс EmployerDao.
 */
public class EmployerDaoImpl implements EmployerDao {
    /**
     * Метод всталяет работодателя в DataBase.
     * @param employer - работник,
     * @return - токен.
     */
    @Override
    public String insert(Employer employer) throws ServiceException {
        return SingDataBase.addEmployer(employer);
    }

    /**
     * Метод добавляет вакансию к нужному работодателю.
     * @param vacancy - вакансия,
     * @param token - токен,
     * @return - результат действия.
     * @throws ServiceException
     */
    @Override
    public String addVacancy(Vacancy vacancy, String token) throws ServiceException {
        return SingDataBase.addVacancy(vacancy, token);
    }

    /**
     * Метод получает список подходящих работников.
     * @param vacancy - вакансия,
     * @param enumRequest - тип запроса,
     * @param token - токен,
     * @return - список подходящих работников.
     * @throws ServiceException
     */
    @Override
    public List<Worker> getWorker(Vacancy vacancy, EnumRequest enumRequest, String token) throws ServiceException {
        return SingDataBase.getWorker(vacancy, enumRequest, token);
    }
}
