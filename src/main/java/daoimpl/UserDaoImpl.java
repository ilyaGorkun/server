package daoimpl;


import dao.UserDao;
import data.SingDataBase;
import serviceException.ServiceException;
import structures.User;

/**
 * Класс имплементирует UserDao.
 */
public class UserDaoImpl implements UserDao {
    /**
     * Метод для выхода пользователя из сервера.
     * @param user - пользоваьель.
     */
    @Override
    public void userLogout(User user) throws ServiceException {
        SingDataBase.logout(user);
    }

    /**
     * Метод для входа в сервер.
     * @param user - пользователь,
     * @return - токен.
     * @throws ServiceException
     */
    @Override
    public String userLogin(User user) throws ServiceException { return SingDataBase.userLogin(user);}

}
