package daoimpl;

import dao.WorkerDao;
import data.SingDataBase;
import enumRequest.EnumRequest;
import serviceException.ServiceException;
import structures.Skill;
import structures.Vacancy;
import structures.Worker;

import java.util.List;

/**
 * Класс имплементирует WorkerDao.
 */
public class WorkerDaoImpl implements WorkerDao {
    /**
     * Метод для вставки работника в DataBase.
     * @param worker - работник,
     * @return - токен.
     * @throws ServiceException
     */
    @Override
    public String insert(Worker worker) throws ServiceException {
        return SingDataBase.addWorker(worker);
    }

    /**
     * Метод получает список пдходящих вакансий.
     * @param listSkill - список навыков,
     * @param enumRequest - тип запроса,
     * @param token - токен,
     * @return - список вакансий.
     * @throws ServiceException
     */
    @Override
    public List<Vacancy> getVacancy(List<Skill> listSkill, EnumRequest enumRequest, String token) throws ServiceException {
        return SingDataBase.getVacancy(listSkill, enumRequest, token);
    }

    /**
     * Метод добавляет навык к нужному работнику.
     * @param skill - навык,
     * @param token - токен,
     * @return - результат действия.
     * @throws ServiceException
     */
    @Override
    public String addSkill(Skill skill, String token) throws ServiceException {
        return SingDataBase.addSkill(skill, token);
    }


}
