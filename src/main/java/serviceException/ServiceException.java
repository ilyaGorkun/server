package serviceException;

import message.Error;

/**
 * Класс, который занимается исключениями.
 */
public class ServiceException extends Exception {
    private Error errorCode;

    public ServiceException(Error errorCode) {
        this.errorCode = errorCode;
    }

    public Error getErrorCode() {
        return errorCode;
    }
}
