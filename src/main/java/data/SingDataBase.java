package data;

import actions.DataBaseProcessor;
import enumRequest.EnumRequest;
import serviceException.ServiceException;
import structures.*;

import java.util.List;

/**
 * Класс одинчока для DataBase.
 */
public class SingDataBase {
    private static SingDataBase sDataBase;
    private DataBase dataBase;

    private SingDataBase(){}

    public static SingDataBase getSingDatabase() {
        if(sDataBase == null) {
            sDataBase = new SingDataBase();
        }
        return sDataBase;
    }

    /**
     * Метод делегирует запись DataBase в файл классу DataBaseProcessor.
     * @param savedDataFileName - имя файла.
     */
    public void addDataBase (String savedDataFileName) {
        this.dataBase = DataBaseProcessor.deserializeDataBaseFromJsonFile(savedDataFileName);
    }

    /**
     * Метод делегирует добавление работодателя в DataBase классу DataBase.
     * @param employer - работодатель.
     */
    public static String addEmployer(Employer employer) throws ServiceException {
        return getSingDatabase().getDataBase().addEmployer(employer);
    }

    /**
     * Метод делегирует добавление работника в DataBase классу DataBase.
     * @param worker - работник,
     * @return - токен работника.
     */
    public static String addWorker(Worker worker) throws ServiceException {
        return getSingDatabase().getDataBase().addWorker(worker);
    }

    /**
     * Метод делегирует добавление вакансии в DataBase классу DataBase.
     * @param vacancy - вакансия,
     * @param token - токен,
     * @return - результат добавления.
     */
    public static String addVacancy(Vacancy vacancy, String token) throws ServiceException {
        return getSingDatabase().getDataBase().addVacancy(vacancy, token);
    }

    /**
     * Метод делегирует добавление навыка в DataBase классу DataBase.
     * @param skill - навык,
     * @param token - токен,
     * @return - результат добавления.
     */
    public static String addSkill(Skill skill, String token) throws ServiceException {
        return getSingDatabase().getDataBase().addSkill(skill, token);
    }

    /**
     * Метод делегирует получение всех нужных вакансий из DataBase классу DataBase.
     * @param listSkill - список навыков,
     * @param enumRequest - тип запроса,
     * @param token - токен,
     * @return - список подходящих вакансий.
     */
    public static List<Vacancy> getVacancy(List<Skill> listSkill, EnumRequest enumRequest, String token) throws ServiceException {
        return getSingDatabase().getDataBase().getVacancy(listSkill, enumRequest, token);
    }

    /**
     * Метод делегирует получения всех нужных работников из DataBase классу DataBase.
     * @param vacancy - вакансия,
     * @param enumRequest - тип запроса,
     * @param token - токен,
     * @return - список всех подходящих.
     */
    public static List<Worker> getWorker(Vacancy vacancy, EnumRequest enumRequest, String token) throws ServiceException {
        return getSingDatabase().getDataBase().getWorker(vacancy, enumRequest, token);
    }

    /**
     * Метод делегирует выход пользоваетля из сервера классу DataBase.
     * @param user - пользователь.
     */
    public static void logout(User user ) throws ServiceException { getSingDatabase().getDataBase().logout(user);
    }

    /**
     * Метод делегирует вход в сервер классу DataBase.
     * @param user - пользователь,
     * @return - токен.
     */
    public static String userLogin(User user) throws ServiceException {
        return  getSingDatabase().getDataBase().login(user);
    }

    public DataBase getDataBase(){
        return this.dataBase;
    }

}
