package data;

import enumRequest.EnumRequest;
import message.Error;
import serviceException.ServiceException;
import structures.*;

import java.util.*;

import static java.util.stream.Collectors.toMap;

/**
 * Класс реализует DataBase, используя коллекции.
 */
public class DataBase {
    /**
     * Коллекция Map, логин-пользователь.
     */
    private Map<String, User> loginToUser;
    /**
     * Коллекция Map, пользователь-токен.
     */
    private Map<User, String> userToToken;
    /**
     * Коллекция Map, токен-пользователь.
     */
    private Map<String,User> tokenToUser;
    /**
     * Коллекция SortedMap, навык-список_работников.
     */
    private SortedMap<Skill, List<Worker>> skillToListWorker;
    /**
     * Коллекция SortedMap, требование-список_вакансий.
     */
    private SortedMap<Skill, List<Vacancy>> skillToListVacancy;

    /**
     * Конструктор - создает объект в одном экземпяре в начале сессии
     */
    public DataBase() {
        this.loginToUser = new HashMap<>();
        this.userToToken = new HashMap<>();
        this.tokenToUser = new HashMap<>();
        this.skillToListVacancy = new TreeMap<>((p1, p2) -> {
            int nameSkillCompareResult = p1.getSkillName().compareTo(p2.getSkillName());
            if (nameSkillCompareResult != 0) {
                return nameSkillCompareResult;
            }
            Integer level1 = p1.getLevel();
            Integer level2 = p2.getLevel();
            return level1.compareTo(level2);
        });
        this.skillToListWorker = new TreeMap<>((p1,p2) -> {
            int nameSkillCompareResult = p1.getSkillName().compareTo(p2.getSkillName());
            if (nameSkillCompareResult != 0) {
                return nameSkillCompareResult;
            }
            Integer level1 = p1.getLevel();
            Integer level2 = p2.getLevel();
            return level1.compareTo(level2);
        });
    }

    /**
     * Метод, который формирует список нужных вакансий по навыкам, выбирая уровень от исходного до максимального.
     * @param listSkill - спиок навыков,
     * @return - список вакансий.
     */
    private List<Vacancy> getVacancyRequestNotLess(List<Skill> listSkill) {
            List<Set<Vacancy>> list = new ArrayList<>();
            for (Skill skill: listSkill) {
                SortedMap<Skill, List<Vacancy>> sub = skillToListVacancy.subMap(skill, new Skill(skill.getSkillName(),6));
                Set<Vacancy> set = new HashSet<>();
                set.addAll(skillToListVacancy.get(skill));
                for (Map.Entry<Skill, List<Vacancy>> entry: sub.entrySet()) {
                    set.addAll(entry.getValue());
                }
                list.add(set);
            }

            for (Set<Vacancy> elem: list) {
                list.get(0).retainAll(elem);
            }

        return new ArrayList<>(list.get(0));
        }

    /**
     * Метод, который формирует список нужных вакансий по навыкам, выбирая только обязательные.
     * @param listSkill - список навыков,
     * @return - список вакансй.
     */
    private List<Vacancy> getVacancyWithRequirement(List<Skill> listSkill) {
        List<Vacancy> list = getVacancyRequestNotLess(listSkill);

        for (Skill skill: listSkill) {
            for (Vacancy vacancy: list) {
                if (vacancy.getListDemand().contains(new Demand(skill.getSkillName(),skill.getLevel(),true))) {
                    vacancy.getListDemand().remove(skill);
                }
            }
        }

        return list;
        }

    /**
     * Метод, который формирует список нужных вакансий по навыкам, выбирая все уровни.
     * @param listSkill - список навыков,
     * @return - список вакансий.
     */
    private List<Vacancy> getVacancyRequestGetAll(List<Skill> listSkill) {
        List<Set<Vacancy>> list = new ArrayList<>();
        for (Skill skill : listSkill) {
            SortedMap<Skill, List<Vacancy>> sub = skillToListVacancy.subMap(new Skill(skill.getSkillName(), 0),
                                                                            new Skill(skill.getSkillName(), 6));
            Set<Vacancy> set = new HashSet<>();
            set.addAll(skillToListVacancy.get(skill));
            for (Map.Entry<Skill, List<Vacancy>> entry : sub.entrySet()) {
                set.addAll(entry.getValue());
            }
            list.add(set);
        }

        for (Set<Vacancy> elem : list) {
            list.get(0).retainAll(elem);
        }

        return new ArrayList<>(list.get(0));
    }

    /**
     * Метод, который формирует список нужных вакансий по навыкам, сортируя по уровню.
     * @param listSkill -  список навыков,
     * @return - список вакансий.
     */
    private List<Vacancy> getVacancyRequestSorted(List<Skill> listSkill) {
        List<Set<Vacancy>> list = new ArrayList<>();
        for (Skill skill: listSkill) {
            SortedMap<Skill, List<Vacancy>> sub = skillToListVacancy.subMap(skill,new Skill(skill.getSkillName(),6));
            Set<Vacancy> set = new HashSet<>();
            set.addAll(skillToListVacancy.get(skill));
            for (Map.Entry<Skill, List<Vacancy>> entry: sub.entrySet()) {
                set.addAll(entry.getValue());
            }
            list.add(set);
        }

        Map<Vacancy, Integer> countVacancy = new HashMap<>();
        for (int i = 0; i < list.size(); i++) {
            for (Vacancy vacancy: list.get(i)) {
                if (!countVacancy.containsKey(vacancy)) {
                    countVacancy.put(vacancy, 1);
                } else {
                    Integer count = countVacancy.get(vacancy);
                    countVacancy.put(vacancy, count++);
                }
            }
        }
        Map<Vacancy,Integer> sorted = countVacancy
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(
                        toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                                LinkedHashMap::new));


        return new ArrayList<>(sorted.keySet());
    }

    /**
     * Метод, который формирует список нужных работников по вакансии, выбирая уровень от исходного до максимального.
     * @param vacancy - вакансия,
     * @return - список работников.
     */
    private List<Worker> getWorkerRequestNotLess(Vacancy vacancy) {
            List<Set<Worker>> list = new ArrayList<>();
            for (Demand demand: vacancy.getListDemand()) {
                SortedMap<Skill, List<Worker>> sub = skillToListWorker.subMap(demand,new Skill(demand.getSkillName(),6));
                Set<Worker> set = new HashSet<>();
                set.addAll(skillToListWorker.get(demand));
                for (Map.Entry<Skill, List<Worker>> entry: sub.entrySet()) {
                    set.addAll(entry.getValue());
                }
                list.add(set);
            }

            for (Set<Worker> elem: list) {
                list.get(0).retainAll(elem);
            }

            return new ArrayList<>(list.get(0));
        }

    /**
     * Метод, который формирует список нужных работников по вакансии, выбирая только обязательные.
     * @param vacancy - вакансия,
     * @return - список работников.
     */
    private List<Worker> getWorkerRequestWithRequirement(Vacancy vacancy) {
            List<Set<Worker>> list = new ArrayList<>();
            for (Demand demand: vacancy.getListDemand()) {
                if (!demand.isRequired()) {
                    continue;
                }
                SortedMap<Skill, List<Worker>> sub = skillToListWorker.subMap(demand, new Skill(demand.getSkillName(),6));
                Set<Worker> set = new HashSet<>();
                set.addAll(skillToListWorker.get(demand));
                for (Map.Entry<Skill, List<Worker>> entry: sub.entrySet()) {
                    set.addAll(entry.getValue());
                }
                list.add(set);
            }

            for (Set<Worker> elem: list) {
                list.get(0).retainAll(elem);
            }

            return new ArrayList<>(list.get(0));
        }

    /**
     * Метод, который формирует список нужных работников по вакансии, выбирая все уровни.
     * @param vacancy - вакансия,
     * @return - список работников.
     */
    private List<Worker> getWorkerRequestGetAll(Vacancy vacancy) {
            List<Set<Worker>> list = new ArrayList<>();
            for (Demand demand: vacancy.getListDemand()) {
                SortedMap<Skill, List<Worker>> sub = skillToListWorker.subMap( new Skill(demand.getSkillName(),0), new Skill(demand.getSkillName(),6));
                Set<Worker> set = new HashSet<>();
                set.addAll(skillToListWorker.get(demand));
                for (Map.Entry<Skill, List<Worker>> entry: sub.entrySet()) {
                    set.addAll(entry.getValue());
                }
                list.add(set);
            }

            for (Set<Worker> elem: list) {
                list.get(0).retainAll(elem);
            }

            return new ArrayList<>(list.get(0));
        }

    /**
     * Метод, который формирует список нужных вакансий по навыкам, хотя бы подному уровню.
     * @param vacancy - вакансия,
     * @return - список работников.
     */
    private List<Worker> getWorkerRequestAtLeastOne(Vacancy vacancy) {
            List<Set<Worker>> list = new ArrayList<>();
            for (Demand demand: vacancy.getListDemand()) {
                SortedMap<Skill, List<Worker>> sub = skillToListWorker.subMap(demand,new Skill(demand.getSkillName(),6));
                Set<Worker> set = new HashSet<>();
                set.addAll(skillToListWorker.get(demand));
                for (Map.Entry<Skill, List<Worker>> entry: sub.entrySet()) {
                    set.addAll(entry.getValue());
                }
                list.add(set);
            }

            for (Set<Worker> elem: list) {
                list.get(0).addAll(elem);
            }

            return new ArrayList<>(list.get(0));

        }

    /**
     * Метод, который проверяет на существование данного пользователя
     * @param login - логин,
     * @return - существование в БД.
     */
    public boolean containsUserInDataBase(String login){
            return loginToUser.containsKey(login);
        }

    /**
     * Метод, который выбирает определенный тип сортировки и поиска.
     * @param listSkill - список навыков,
     * @param enumRequest - тип запроса,
     * @param token - токен,
     * @return - список вакансий.
     */
        public List<Vacancy> getVacancy(List<Skill> listSkill, EnumRequest enumRequest, String token) throws ServiceException {

            if(!tokenToUser.containsKey(token)) {
                throw new ServiceException(Error.WRONG_FALSE_TOKEN);
            }

            if (!tokenToUser.get(token).getClass().equals(Worker.class)) {
                throw new ServiceException(Error.WRONG_WORKER_NOT_EXISTS);
            }

            switch (enumRequest) {
                case REQUEST_GET_NOT_LESS:
                    return getVacancyRequestNotLess(listSkill);
                case REQUEST_WITH_REQUIREMENT:
                    return  getVacancyWithRequirement(listSkill);
                case REQUEST_GET_ALL:
                    return getVacancyRequestGetAll(listSkill);
                case REQUEST_GET_NOT_LESS_SORTED:
                    return getVacancyRequestSorted(listSkill);
                    default:
                        return null;
            }

        }

    /**
     * Метод, который выбирает определенный тип сортировки и поиска.
     * @param vacancy - вакансия,
     * @param enumRequest - тип запроса,
     * @param token - токен,
     * @return - список работников.
     */
        public List<Worker> getWorker(Vacancy vacancy, EnumRequest enumRequest, String token) throws ServiceException {
            if(!tokenToUser.containsKey(token)) {
                throw new ServiceException(Error.WRONG_FALSE_TOKEN);
            }

            if (!tokenToUser.get(token).getClass().equals(Employer.class)) {
                throw new ServiceException(Error.WRONG_EMPLOYER_EXISTS);
            }

            switch (enumRequest) {
                case REQUEST_GET_NOT_LESS:
                    return getWorkerRequestNotLess(vacancy);
                case REQUEST_WITH_REQUIREMENT:
                    return getWorkerRequestWithRequirement(vacancy);
                case REQUEST_GET_ALL:
                    return getWorkerRequestGetAll(vacancy);
                case REQUEST_AT_LEAST_ONE:
                    return getWorkerRequestAtLeastOne(vacancy);
                    default:
                        return null;
            }
        }

    /**
     * Добавляет работодателя в БД.
     * @param employer - работодатель,
     * @return - токен.
     */
        public String addEmployer(Employer employer) throws ServiceException {

            if(containsUserInDataBase(employer.getLogin())) {
            	throw new ServiceException(Error.WRONG_LOGIN_IS_EXISTS);
            }

            String token = UUID.randomUUID().toString();

            loginToUser.put(employer.getLogin(), employer);
            tokenToUser.put(token, employer);
            return token;

        }

    /**
     * Метод добавляет работника в БД.
     * @param worker - работник,
     * @return - токен.
     */
        public String addWorker(Worker worker) throws ServiceException {
            if (containsUserInDataBase(worker.getLogin())){
                throw new ServiceException(Error.WRONG_LOGIN_IS_EXISTS);
            }

            String token = UUID.randomUUID().toString();

            for (Skill skill: worker.getListSkill()) {
                if(skillToListWorker.containsKey(skill)) {
                    skillToListWorker.get(skill).add(worker);
                } else {
                    skillToListWorker.put(skill, new ArrayList<>());
                    skillToListWorker.get(skill).add(worker);
                }
            }

            tokenToUser.put(token, worker);
            loginToUser.put(worker.getLogin(), worker);

            return token;

        }

    /**
     * Метод добавляет навыков.
     * @param skill - навык,
     * @param token - токен,
     * @return - результат добавления.
     */
        public String addSkill(Skill skill, String token) throws ServiceException {
            if (!tokenToUser.containsKey(token)) {
                throw new ServiceException(Error.WRONG_FALSE_TOKEN);
            }

            if (!tokenToUser.get(token).getClass().equals(Worker.class)) {
                throw new ServiceException(Error.WRONG_WORKER_NOT_EXISTS);
            }
            Worker worker = (Worker) tokenToUser.get(token);
            if (worker.getListSkill().contains(skill)) {
                throw new ServiceException(Error.WRONG_SKILL_EXISTS);
            }
            worker.getListSkill().add(skill);
            return "";
        }

    /**
     * Метод добавляет вакансию.
     * @param vacancy - вакансия,
     * @param token - токен,
     * @return - результат добавления.
     */
        public String addVacancy(Vacancy vacancy, String token) throws ServiceException {

            if (!tokenToUser.containsKey(token)) {
                throw new ServiceException(Error.WRONG_FALSE_TOKEN);
            }

            if(!tokenToUser.get(token).getClass().equals(Employer.class)){
                throw new ServiceException(Error.WRONG_EMPLOYER_NOT_EXISTS);
            }
            for (Demand demand: vacancy.getListDemand()) {

                if(skillToListVacancy.containsKey(demand)) {
                    skillToListVacancy.get(demand).add(vacancy);
                } else {
                    skillToListVacancy.put(demand, new ArrayList<>());
                    skillToListVacancy.get(demand).add(vacancy);
                }

            }



            Employer employer = (Employer) tokenToUser.get(token);
            if(employer.getListVacancy().contains(vacancy)) {
                throw new ServiceException(Error.WRONG_VACANCY_EXISTS);
            }
            employer.getListVacancy().add(vacancy);
            return "";
        }

    /**
     * Выход пользователя из БД и удаление его токена.
     * @param user - пользователь.
     */
        public void logout(User user) throws ServiceException {
        if(userToToken.containsKey(user)) {
            throw new ServiceException(Error.WRONG_LOGOUT);
        }
        userToToken.remove(user);
        }

    /**
     * Вход пользователя и создание нового токена.
     * @param user - пользователь,
     * @return - токен.
     */
        public String login(User user) throws ServiceException {
            if (!loginToUser.containsKey(user.getLogin())) {
                throw new ServiceException(Error.WRONG_LOGIN_NOT_EXISTS);
            }
            String token = UUID.randomUUID().toString();
            userToToken.put(user, token);
            return token;
        }

}


